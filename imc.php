<!--  OUVERTURE DE BALISE PHP  -->
<?php
/*creation de la class Personne  */ 
class Personne {
    /* creation des variable */ 
    public float $taille;
    public float $poids;
    /* premiére fonction construc qui inisialiser les varriable*/ 
    public function __construct(float $tailleEnMetre, float $poidsEnKilos){
        $this->taille = $tailleEnMetre;
        $this->poids = $poidsEnKilos;
    }
    /* fonction get qui va recupere une valeur */
    public function getTaille():float{
        return $this->taille;
    }
    /* fonction get qui va recupere une valeur */
    public function getPoids():float{
        return $this->poids;
    }
    /* fonction set qui va inisialiser une valeur */
    public function setTaille(float $taille):void{
        $this->taille = $taille;
    }
    /* fonction set qui va inisialiser une valeur */
    public function setPoids(float $poids):void{
        $this->poids = $poids;
    }
    /* fonction IMC qui vas calculer et renvoyer la valeur le l'imc de la personne */
    public function IMC():float {
        /* calcul de l imc*/
        $imc = $this->poids/($this->taille*$this->taille);
        /* fonction if qui va veriffier si la question est corect */ 
        if ($imc<18.5){
            /* si la reponse est corect alors on renvois le contenu de l'echo de maniére visible sur le site */ 
            echo "trop maigre";
        } else if ($imc>18.5 && $imc<25){
            echo "bien";
        } else {
            echo"trop gros";
        }
        /* retourne le resulta de imc */
        return $imc;
    }
}
/* on declare une nouvelle personne d'1.75m et de 80kg */
$perImc = new Personne(1.65 , 65);
/* affichage du resulta*/
echo 'votre imc est '. $perImc->IMC();
?>